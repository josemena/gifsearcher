//
//  ViewController.swift
//  GIFSearcher
//
//  Created by Viviana Uscocovich-Mena on 11/20/17.
//  Copyright © 2017 Jose Mena. All rights reserved.
//

import UIKit

class GIFSearcherViewController: UIViewController {

    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var GIFCollectionView: UICollectionView!
    @IBOutlet weak var searchedTableView: UITableView!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    
    let viewModel = GIFSearcherViewModel()
    var layout: MosaicLayout?
    var searchedTerms = [String]()          //to keep track of all previous searches
    var matchedSearchTerms = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchedTableView.delegate = self
        searchedTableView.dataSource = self
        searchBar.delegate = self
        GIFCollectionView.dataSource = viewModel
        GIFCollectionView.prefetchDataSource = viewModel
        layout = GIFCollectionView.collectionViewLayout as? MosaicLayout
        layout?.delegate = viewModel
        viewModel.fetchTrendingGifs {
            self.GIFCollectionView.reloadData()
        }
        tableViewHeightConstraint.constant = 0
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        showToast(message: "Trending GIFs")
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        print("view transitioned \(size)")
        layout?.invalidateLayout()
        self.GIFCollectionView.reloadData()
    }
    
    func showToast(message: String) {
        let frame = CGRect(x: 0,
                           y: self.searchBar.frame.origin.y + self.searchBar.frame.height,
                           width: self.view.frame.width,
                           height: 35)
        
        let label = UILabel(frame: frame)
        label.backgroundColor = UIColor.black.withAlphaComponent(0.9)
        label.textColor = UIColor.white
        label.textAlignment = NSTextAlignment.center
        label.font = UIFont(name: "System", size: 17.0)
        label.text = message
        label.alpha = 1.0
        label.layer.cornerRadius = 10
        label.clipsToBounds = true
        self.view.addSubview(label)
        UIView.animate(withDuration: 10.0,
                       delay: 0.1,
                       options: .curveEaseOut,
                       animations: {
                        label.alpha = 0.0
                        var frame = label.frame
                        frame.origin.y = 0
                        label.frame = frame
            }, completion: {(isCompleted) in
                label.removeFromSuperview()
                
        })
    }

}

extension GIFSearcherViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {

        self.view.endEditing(true)
        layout?.invalidateLayout()
        self.GIFCollectionView.collectionViewLayout.prepare()
        guard let searchString = searchBar.text else {
            return
        }
        if searchString.isEmpty {
            viewModel.fetchTrendingGifs {
                self.GIFCollectionView.reloadData()
            }
            return
        }
        
        searchedTerms.append(searchString)
        viewModel.fetch(searchString: searchString) {
            self.GIFCollectionView.reloadData()
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        //every time text changes rebuild the matchedSearchTerms array
        matchedSearchTerms.removeAll()
        guard let searchString = searchBar.text else {
            return
        }
        
        for searchTerm in searchedTerms {
            if searchTerm.contains(searchString) {
                matchedSearchTerms.append(searchTerm)
            }
        }
        searchedTableView.reloadData()
        tableViewHeightConstraint.constant = searchedTableView.contentSize.height
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchedTableView.isHidden = false
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchedTableView.isHidden = true
    }
}

extension GIFSearcherViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchedTermCell", for: indexPath)
        cell.textLabel?.text = matchedSearchTerms[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return matchedSearchTerms.count
    }
}

extension GIFSearcherViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let searchString = matchedSearchTerms[indexPath.row]
        searchBar.text = searchString
        matchedSearchTerms.removeAll()
        tableView.reloadData()
        tableViewHeightConstraint.constant = tableView.contentSize.height
        viewModel.fetch(searchString: searchString) {
            self.GIFCollectionView.reloadData()
        }
    }
}
