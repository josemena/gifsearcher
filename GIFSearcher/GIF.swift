//
//  GIF.swift
//  GIFSearcher
//
//  Created by Viviana Uscocovich-Mena on 11/21/17.
//  Copyright © 2017 Jose Mena. All rights reserved.
//

import UIKit
class GIF {
    
    let gifInfo: [String: Any]
    
    public init(json: [String: Any]) {
        gifInfo = json
    }
    
    public class func createGIFS(json: [String: Any]) -> [GIF] {
        var gifs = [GIF]()
        guard let data = json["data"] as? [[String: Any]] else {
                return gifs
        }
        
        for jsonGifData in data {
            let gif = GIF(json: jsonGifData)
            gifs.append(gif)
        }
        return gifs
    }
    
    private var imageInfo: [String: Any]? {
        guard let images = gifInfo["images"] as? [String: Any],
            let imageInfo = images["fixed_height"] as? [String: Any] else {
                return nil
        }
        return imageInfo
    }
    public var url: String {
        guard let urlString = imageInfo?["url"] as? String else {
            return ""
        }
    
        return urlString
    }
    
    public var width: CGFloat {
        guard let widthStr = imageInfo?["width"] as? String else {
            return 0
        }
        if let w = Float(widthStr) {
            return CGFloat(w)
        }
        return 0
    }
    
    public var height: CGFloat {
        guard let heightStr = imageInfo?["height"] as? String else {
            return 0
        }
        if let h = Float(heightStr) {
            return CGFloat(h)
        }
        return 0
    }
}
