//
//  NetworkClient.swift
//  GIFSearcher
//
//  Created by Viviana Uscocovich-Mena on 11/21/17.
//  Copyright © 2017 Jose Mena. All rights reserved.
//

import Foundation

class NetworkClient {
    internal let urlString : String
    internal let session = URLSession.shared
    internal let APIKey = "wpA4zc4Z3XTximgsa06WMh9Ga3O0EnuO"
    
    public static let shared: NetworkClient = {
        return NetworkClient(urlString: "https://api.giphy.com/v1/gifs")
    }()
    
    private init(urlString: String) {
        self.urlString = urlString
    }
    
    private func fetch(endpoint: String,
                       success: @escaping([GIF]) -> Void,
                       failure: @escaping(Error?) -> Void) {
        let urlString = "\(self.urlString)/\(endpoint)api_key=\(APIKey)"
        let url = URL(string: urlString)!
        let task = session.dataTask(with: url, completionHandler: { (data, response, error) in
            
            if let error = error {
                DispatchQueue.main.async {
                    failure(error)
                }
            }
            
            guard let httpResponse = response as? HTTPURLResponse,
                httpResponse.statusCode == 200,
                let data = data,
                let jsonObject = try? JSONSerialization.jsonObject(with: data) else {
                    print("Error fetching data")
                    DispatchQueue.main.async {
                        failure(error)
                    }
                    return
            }
            
            print(httpResponse.statusCode)
            if let json = jsonObject as? [String: Any] {
                let gifs = GIF.createGIFS(json: json)
                DispatchQueue.main.async {
                    success(gifs)
                }
            }
        })
        
        task.resume()
    }
    
    public func fetchTrending(success: @escaping ([GIF]) -> Void,
                              failure: @escaping (Error?) -> (Void)) {
        fetch(endpoint: "trending?", success: success, failure: failure)
    }
    
    public func fetchSearch(queryString: String,
                            success: @escaping ([GIF]) -> Void,
                            failure: @escaping (Error?) -> (Void)) {
        fetch(endpoint: "search?q=\(queryString)&", success: success, failure: failure)
        
    }
}

