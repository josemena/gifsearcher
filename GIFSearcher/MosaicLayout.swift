//
//  MosaicLayout.swift
//  GIFSearcher
//
//  Created by Viviana Uscocovich-Mena on 11/25/17.
//  Copyright © 2017 Jose Mena. All rights reserved.
//

import UIKit

protocol MosaicLayoutDelegate {
    func collectionView(_ collectionView: UICollectionView, sizeForItemAt indexPath: IndexPath) -> CGSize
}

class MosaicLayout: UICollectionViewLayout {
    var columnCount = 2
    let inset: CGFloat = 8.0
    let spacing: CGFloat = 8.0
    let verticalSpacing: CGFloat = 8.0
    
    var delegate: MosaicLayoutDelegate!
    
    private var cache = [UICollectionViewLayoutAttributes]()
    private var contentHeight: CGFloat = 0
    private var width: CGFloat {
        get {
            return collectionView!.bounds.width
        }
    }
    
    override var collectionViewContentSize: CGSize {
        return CGSize(width: width, height: contentHeight)
    }
    
    override func prepare() {
        super.prepare()
        if cache.isEmpty {
            let columnWidth = ((width - (inset + spacing)) / CGFloat(columnCount))
            
            var xOffsets = [CGFloat]()
            for column in 0..<columnCount {
                xOffsets.append(CGFloat(column) * (columnWidth + spacing) + (inset / CGFloat(columnCount)))
            }
            
            var yOffsets = [CGFloat](repeating: 0, count: columnCount)
            
            var column = 0
            for item in 0..<collectionView!.numberOfItems(inSection: 0) {
                let indexPath = IndexPath(item: item, section: 0)
                let size = delegate.collectionView(collectionView!, sizeForItemAt: indexPath)
                
                //size returns actual width of object, scale it to fit in the wanted size
                let scale = columnWidth / size.width
                let scaledHeight = size.height * scale
                
                let frame = CGRect(x: xOffsets[column],
                                   y: yOffsets[column],
                                   width: columnWidth,
                                   height: scaledHeight)
                let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
                attributes.frame = frame
                cache.append(attributes)
                
                contentHeight = max(contentHeight, frame.maxY)
                yOffsets[column] = yOffsets[column] + scaledHeight + verticalSpacing
                column += 1
                column %= columnCount
            }
        }
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var layoutAttributes = [UICollectionViewLayoutAttributes]()
        for attributes in cache {
            if attributes.frame.intersects(rect) {
                layoutAttributes.append(attributes)
            }
        }
        return layoutAttributes
    }
    
    override func invalidateLayout() {
        super.invalidateLayout()
        cache.removeAll()
    }
}
